import hashlib

from flask import Blueprint
from flask import render_template, request

from scripts.service.email_service import sender
from scripts.handler.login_handler import Mongobd

blueprint_inserter = Blueprint('example_blueprint', __name__)

mongodb_object = Mongobd()


@blueprint_inserter.route('/login')
def message():
    return render_template('login.html')


@blueprint_inserter.route('/registration')
def message1():
    return render_template('registration.html')


@blueprint_inserter.route('/registration_success')
def message2():
    return render_template('registration_success.html')


@blueprint_inserter.route('/registration_data', methods=['POST'])
def input_values():
    user_entered_password = request.form["password"]
    salt = "5gz"
    db_password = user_entered_password + salt
    h = hashlib.md5(db_password.encode())
    data = {"Name": request.form["name"],
            "Phone number": request.form["number"],
            "Mail id": request.form["mail"],
            "User Id": request.form["user_id"],
            "Password": h.hexdigest()}
    result = mongodb_object.find_query_register(data["User Id"])
    if result == 1:
        error = "Existing user"
        return render_template('login.html', error=error)

    mongodb_object.insert(data)
    return render_template('registration_success.html')


@blueprint_inserter.route('/login_data', methods=['POST'])
def required_values():
    result = mongodb_object.find_query(request.form["username"], request.form["pwd"])
    if result[0] == 1:
        sender(result[2], request.form["username"])
        return render_template('login_success.html')
    elif result[1] == 0:
        error = "No user found"
        return render_template('login.html', error=error)
    else:
        error = "Wrong Password"
        return render_template('login.html', error=error)
