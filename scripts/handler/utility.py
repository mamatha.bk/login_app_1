
from pymongo import MongoClient


class Mongoutility:
    # db connection
    @staticmethod
    def connection():
        try:
            client = MongoClient('localhost', 27017)
            database = client.mamatha
            register_details = database.register_details
            return register_details
        except Exception:
            print("")

    # data insertion
    @staticmethod
    def insert(register_details, data):
        try:
            # we can use insert_many for insertion all records at a time.
            register_details.insert(data)
        except Exception:
            print("")

    # finding data
    @staticmethod
    def finding(register_details, query):
        details = register_details.find(query)
        return details
