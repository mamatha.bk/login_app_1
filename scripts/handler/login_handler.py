from scripts.handler.utility import Mongoutility
import hashlib

utility_object = Mongoutility()
register_details = utility_object.connection()


class Mongobd:

    # inserting data containing all fields of a person
    @staticmethod
    def insert(data):
        utility_object.insert(register_details, data)

    # searching for particular user login
    @staticmethod
    def find_query(username, password):
        my_cursor = utility_object.finding(register_details, {"User Id": username})
        flag = 0
        flag1 = 0
        user_mail = ""
        # salting and hashing
        user_entered_password = password
        salt = "5gz"
        db_password = user_entered_password + salt
        h = hashlib.md5(db_password.encode())
        # try block used if there is no particular user in db
        try:
            # user validation
            for key1, value1 in my_cursor.next().items():
                if key1 == "Mail id":
                    user_mail = value1
                if key1 == "User Id" and value1 == username:
                    flag1 = 1
                    break
            # if user found then password validation
            my_cursor = utility_object.finding(register_details, {"User Id": username})
            for key, value in my_cursor.next().items():
                if key == "Password" and value == h.hexdigest():
                    flag = 1
        except Exception:
            print("")
        return flag, flag1, user_mail

    # duplicate registration validation
    @staticmethod
    def find_query_register(username):
        my_cursor = utility_object.finding(register_details, {"User Id": username})
        flag1 = 0
        try:
            for key1, value1 in my_cursor.next().items():
                if key1 == "User Id" and value1 == username:
                    flag1 = 1

        except Exception:
            print("")
        return flag1
